#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

long long int factorialArr[ROW1][COL2];

void showMatrix(int val[ROW1][COL2]) {
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      printf("%d ", val[i][j]);
    }
    printf("\n");
  }
}

void *factorialThread(void *arg) {
  int *args = (int *)arg;
  int val = args[0];
  int i = args[1];
  int j = args[2];
  long long int result = 1;
  for (int i = 1; i <= val; i++) {
    result *= i;
  }
  factorialArr[i][j] = result;
  free(arg);
  pthread_exit(NULL);
}

int main() {
  clock_t start, end;
  double elapsed_time;
  int(*val)[COL2];
  pthread_t factorial[ROW1 * COL2];
  key_t key = ftok("./kalian.c", 'R');
  int shmid = shmget(key, 4 * 5 * sizeof(int), IPC_CREAT | 0666);
  if (shmid < 0) {
    perror("shmget");
    exit(1);
  }
  val = shmat(shmid, NULL, 0);
  showMatrix(val);
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      int *args = (int *)malloc(3 * sizeof(int));
      args[0] = val[i][j];
      args[1] = i;
      args[2] = j;
      pthread_create(&factorial[i * COL2 + j], NULL, factorialThread,
                     (void *)args);
    }
  }
  start = clock();
  for (int i = 0; i < ROW1 * COL2; i++) {
    pthread_join(factorial[i], NULL);
  }
  end = clock();
  shmdt(val);

  printf("Factorial Result\n");

  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      printf("%lld ", factorialArr[i][j]);
    }
    printf("\n");
  }
  printf("Time Used: %f seconds", elapsed_time);

  return 0;
}
