#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include <unistd.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
int mat1[ROW1][COL1], mat2[ROW2][COL2];

void generateMatrix() {
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL1; j++) {
      mat1[i][j] = rand() % 5 + 1;
    }
  }
  for (int i = 0; i < ROW2; i++) {
    for (int j = 0; j < COL2; j++) {
      mat2[i][j] = rand() % 4 + 1;
    }
  }
}

int main() {
  key_t key = ftok("./kalian.c", 'R');
  int(*val)[COL2];

  int shmid = shmget(key, ROW1 * COL2 * sizeof(int), IPC_CREAT | 0666);
  if (shmid == -1) {
    perror("shmget");
    exit(EXIT_FAILURE);
  }
  val = (int(*)[COL2])shmat(shmid, NULL, 0);
  srand(time(0));
  generateMatrix();
  while (1) {
    int flag = 0;
    for (int i = 0; i < ROW1; i++) {
      for (int j = 0; j < COL2; j++) {
        val[i][j] = 0;
        for (int k = 0; k < ROW2; k++) {
          val[i][j] += (mat1[i][k] * mat2[k][j]);
          if (val[i][j] > 20) {
            flag = 1;
            generateMatrix();
            break;
          }
        }
        if (flag == 1)
          break;
      }
      if (flag == 1)
        break;
    }
    if (flag == 0) {
      break;
    }
  }
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      printf("%d ", val[i][j]);
    }
    printf("\n");
  }
  shmdt(val);
}
