#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include <unistd.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

void showMatrix(int val[ROW1][COL2]) {
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      printf("%d ", val[i][j]);
    }
    printf("\n");
  }
}

long long int factorialWithoutThread(int val) {
  long long int result = 1;
  for (int i = 1; i <= val; i++) {
    result *= i;
  }
  return result;
}

int main() {
  clock_t start, end;
  double elapsed_time;
  long long int factorialArr[ROW1][COL2];
  int(*val)[COL2];
  key_t key = ftok("./kalian.c", 'R');
  int shmid = shmget(key, 4 * 5 * sizeof(int), IPC_CREAT | 0666);
  if (shmid < 0) {
    perror("shmget");
    exit(1);
  }
  val = shmat(shmid, NULL, 0);
  showMatrix(val);
  start = clock();
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      long long int res = factorialWithoutThread(val[i][j]);
      factorialArr[i][j] = res;
    }
  }
  end = clock();
  shmdt(val);

  printf("\nFactorial Result\n");

  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      printf("%lld ", factorialArr[i][j]);
    }
    printf("\n");
  }
  elapsed_time = (double)(end - start) / CLOCKS_PER_SEC;
  printf("Time spent : %f", elapsed_time);
  return 0;
}
