#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

typedef struct MinHeapNode
{
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
} MinHeapNode;

typedef struct MinHeap
{
    unsigned size;
    unsigned capacity;
    MinHeapNode **array;
} MinHeap;

MinHeapNode *newNode(char data, unsigned freq)
{
    MinHeapNode *temp = (MinHeapNode *)malloc(sizeof(MinHeapNode));
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
    return temp;
}

MinHeap *createMinHeap(unsigned capacity)
{
    MinHeap *minHeap = (MinHeap *)malloc(sizeof(MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (MinHeapNode **)malloc(minHeap->capacity * sizeof(MinHeapNode *));
    return minHeap;
}

void swapMinHeapNode(MinHeapNode **a, MinHeapNode **b)
{
    MinHeapNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(MinHeap *minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int isSizeOne(MinHeap *minHeap)
{
    return (minHeap->size == 1);
}

MinHeapNode *extractMin(MinHeap *minHeap)
{
    MinHeapNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}

void insertMinHeap(MinHeap *minHeap, MinHeapNode *minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq)
    {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(MinHeap *minHeap)
{
    int n = minHeap->size - 1;
    int i;
    for (i = (n - 1) / 2; i >= 0; --i)
    {
        minHeapify(minHeap, i);
    }
}

bool isLeaf(MinHeapNode *root)
{
    return !(root->left) && !(root->right);
}

MinHeap *createAndBuildMinHeap(char data[], int freq[], int size)
{
    MinHeap *minHeap = createMinHeap(size);
    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);
    minHeap->size = size;
    buildMinHeap(minHeap);
    return minHeap;
}

MinHeapNode *buildHuffmanTree(char data[], int freq[], int size)
{
    MinHeapNode *left, *right, *top;
    MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);
    while (!isSizeOne(minHeap))
    {
        left = extractMin(minHeap);
        right = extractMin(minHeap);
        top = newNode('$', left->freq + right->freq);
        top->left = left;
        top->right = right;
        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}

void printArr(int arr[], int n)
{
    for (int i = 0; i < n; ++i)
        printf("%d", arr[i]);
    printf("\n");
}

int isUpper(int ch)
{
    return (ch >= 'A' && ch <= 'Z');
}

int toAscii(char ch)
{
    return (int)ch;
}

int toUpper(int ch)
{
    if (ch >= 'a' && ch <= 'z')
    {
        return ch - 'a' + 'A';
    }
    return ch;
}

void calculateFreq(char *str, int *freq)
{
    for (int i = 0; str[i]; i++)
    {
        if (isUpper(toUpper(str[i])))
        {
            freq[toAscii(toUpper(str[i]))]++;
        }
    }
}

void printCodes(MinHeapNode *root, int arr[], int top, FILE *output_file)
{
    if (root->left)
    {
        arr[top] = 0;
        printCodes(root->left, arr, top + 1, output_file);
    }
    if (root->right)
    {
        arr[top] = 1;
        printCodes(root->right, arr, top + 1, output_file);
    }
    if (isLeaf(root))
    {
        fprintf(output_file, "%c: ", root->data);
        printArr(arr, top);
    }
}

void compressHuffman(char *data, int *freq, int size)
{
    MinHeapNode *root = buildHuffmanTree(data, freq, size);
    int arr[100], top = 0;
    FILE *output_file = fopen("file.txt", "w");
    printCodes(root, arr, top, output_file);
    fclose(output_file);
}

int main()
{
    int fd1[2];
    int fd2[2];

    if (pipe(fd1) == -1 || pipe(fd2) == -1)
    {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }

    pid_t pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "Fork Failed");
        return 1;
    }

    if (pid > 0)
    {
        close(fd1[0]);

        FILE *input_file = fopen("file.txt", "r");
        fseek(input_file, 0, SEEK_END);
        long input_file_size = ftell(input_file);
        fseek(input_file, 0, SEEK_SET);
        char *input_data = (char *)malloc(input_file_size + 1);
        fread(input_data, input_file_size, 1, input_file);
        fclose(input_file);
        input_data[input_file_size] = '\0';

        int freq[128] = {0};
        calculateFreq(input_data, freq);

        write(fd1[1], freq, sizeof(freq));
        close(fd1[1]);

        wait(NULL);

        close(fd2[1]);
        read(fd2[0], freq, sizeof(freq));

        char data[128];
        int j = 0;
        for (int i = 0; i < ARRAY_SIZE(freq); i++)
        {
            if (freq[i])
            {
                data[j++] = (char)i;
            }
        }

        compressHuffman(data, freq, j);

        close(fd2[0]);

        // Menghitung jumlah bit sebelum kompresi
        int total_bits_before = 0;
        for (int i = 0; i < ARRAY_SIZE(freq); i++) {
            total_bits_before += freq[i] * 8; // Menghitung jumlah bit untuk setiap karakter
        }

        // Mengompresi file menjadi file.zip
        system("zip file.zip file.txt");

        // Menghitung jumlah bit setelah kompresi
        FILE *compressed_file = fopen("file.zip", "rb");
        fseek(compressed_file, 0, SEEK_END);
        long compressed_file_size = ftell(compressed_file);
        fseek(compressed_file, 0, SEEK_SET);
        int total_bits_after = compressed_file_size * 8;

        printf("Jumlah bit sebelum kompresi: %d\n", total_bits_before);
        printf("Jumlah bit setelah kompresi: %d\n", total_bits_after);
        printf("Perbandingan jumlah bit: %.2f\n", (float)total_bits_after / total_bits_before);

        fclose(compressed_file);
    }
    else
    {
        close(fd1[1]);

        int freq[128] = {0};
        read(fd1[0], freq, sizeof(freq));
        close(fd1[0]);

        for (int i = 0; i < ARRAY_SIZE(freq); i++)
        {
            if (freq[i])
            {
                freq[i] /= 2;
            }
        }

        close(fd2[0]);
        write(fd2[1], freq, sizeof(freq));
        close(fd2[1]);
    }

    return 0;
}
