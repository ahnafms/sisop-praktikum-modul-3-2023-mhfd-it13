# sisop-praktikum-modul-3-2023-mh-it13
## Daftar Isi ##
- [anggota kelompok](#anggota-kelompok)
- [nomor 1](#nomor-1)
    - [soal  1.a](#1a)
    - [soal  1.b](#1b)
    - [soal  1.c](#1c)
    - [soal  1.d](#1d)
- [nomor 2](#nomor-2)
    - [soal  2.1](#21)
    - [soal  2.2](#22)
    - [soal  2.3](#23)
    - [soal  2.4](#24)
- [nomor 3](#nomor-3)
    - [soal  3.a](#3a)
    - [soal  3.b](#3b)
    - [hasil](#3hasil)
- [nomor 4](#nomor-4)
    - [soal  4.a](#3a)
    - [soal  4.b](#3b)
    - [soal  4.c](#3c)
- [kendala](#kendala)


## anggota kelompok

| nrp        | nama                       |
| ---------- | -------------------------- |
| 5027211038 | ahnaf musyaffa             |
| 5027211051 | wisnu adjie saka           |
| 5027211062 | anisa ghina salsabila      |

## nomor 1
Membuat program kompresi file.txt menggunakan Algoritma Huffman untuk proses kompresi lossless. Syarat program : 
1. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
2. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
3. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
4. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
5. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

Catatan:
- Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
Huruf A	: 01000001
Huruf a	: 01100001
- Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
- Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

## 1.1 
```C
if (pid > 0)
{
    close(fd1[0]);

    FILE *input_file = fopen("file.txt", "r");
    fseek(input_file, 0, SEEK_END);
    long input_file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);
    char *input_data = (char *)malloc(input_file_size + 1);
    fread(input_data, input_file_size, 1, input_file);
    fclose(input_file);
    input_data[input_file_size] = '\0';

    int freq[128] = {0};
    calculateFreq(input_data, freq);

    write(fd1[1], freq, sizeof(freq));
    close(fd1[1]);

    wait(NULL);

    close(fd2[1]);
    read(fd2[0], freq, sizeof(freq));
    // ...
}
```
Penjelasan : 
- File input dibuka dan dibaca, dan data disimpan dalam input_data.
- Frekuensi kemunculan setiap huruf dihitung dengan fungsi calculateFreq dan disimpan dalam array freq.
- Array frekuensi freq dikirim ke child process melalui pipe fd1 dengan fungsi write().
- Menunggu child process selesai dengan fungsi wait(NULL).
- Membaca array frekuensi yang telah diubah oleh child process melalui pipe fd2 dengan fungsi read().

## 1.2
```C
    if (pid > 0)
    {
        ...
        // Setelah menerima frekuensi dari child process
        close(fd2[1]);
        read(fd2[0], freq, sizeof(freq));

        char data[128];
        int j = 0;
        for (int i = 0; i < ARRAY_SIZE(freq); i++)
        {
            if (freq[i])
            {
                data[j++] = (char)i;
            }
        }

        // Memanggil fungsi compressHuffman() untuk melakukan kompresi Huffman
        compressHuffman(data, freq, j);

        close(fd2[0]);

        ...
    }

```
Fungsi compressHuffman() berisi proses utama dari algoritma Huffman:
```C
void compressHuffman(char *data, int *freq, int size)
{
    MinHeapNode *root = buildHuffmanTree(data, freq, size);
    int arr[100], top = 0;
    FILE *output_file = fopen("file.txt", "w");
    printCodes(root, arr, top, output_file);
    fclose(output_file);
}
```
Dalam fungsi compressHuffman(), Huffman tree dibangun berdasarkan data dan frekuensi yang diberikan, lalu kode Huffman untuk setiap karakter ditulis ke file output.

## 1.3
Pembuatan Huffman Tree:
```C
MinHeapNode *buildHuffmanTree(char data[], int freq[], int size)
{
    MinHeapNode *left, *right, *top;
    MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);
    while (!isSizeOne(minHeap))
    {
        left = extractMin(minHeap);
        right = extractMin(minHeap);
        top = newNode('$', left->freq + right->freq);
        top->left = left;
        top->right = right;
        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}

```
Konversi setiap karakter ke kode Huffman:
```C
void compressHuffman(char *data, int *freq, int size)
{
    MinHeapNode *root = buildHuffmanTree(data, freq, size);
    int arr[100], top = 0;
    FILE *output_file = fopen("file.txt", "w");
    printCodes(root, arr, top, output_file);
    fclose(output_file);
}

```
Mengirimkan kode Huffman ke program dekompresi:
```C
else
{
    close(fd1[1]);

    int freq[128] = {0};
    read(fd1[0], freq, sizeof(freq));
    close(fd1[0]);

    for (int i = 0; i < ARRAY_SIZE(freq); i++)
    {
        if (freq[i])
        {
            freq[i] /= 2;
        }
    }

    close(fd2[0]);
    write(fd2[1], freq, sizeof(freq));
    close(fd2[1]);
}

```
### 1.4
```C
    close(fd2[0]);
    write(fd2[1], freq, sizeof(freq)); 
    close(fd2[1]);

```
### 1.5
```C
// Menghitung jumlah bit sebelum kompresi
int total_bits_before = 0;
for (int i = 0; i < ARRAY_SIZE(freq); i++) {
    total_bits_before += freq[i] * 8; // Menghitung jumlah bit untuk setiap karakter
}

// Mengompresi file menjadi file.zip
system("zip file.zip file.txt");

// Menghitung jumlah bit setelah kompresi
FILE *compressed_file = fopen("file.zip", "rb");
fseek(compressed_file, 0, SEEK_END);
long compressed_file_size = ftell(compressed_file);
fseek(compressed_file, 0, SEEK_SET);
int total_bits_after = compressed_file_size * 8;

printf("Jumlah bit sebelum kompresi: %d\n", total_bits_before);
printf("Jumlah bit setelah kompresi: %d\n", total_bits_after);
printf("Perbandingan jumlah bit: %.2f\n", (float)total_bits_after / total_bits_before);

fclose(compressed_file);

```

### 1.hasil
Disini terlihat, untuk file.txtnya sudah dizip

![1](/uploads/8bd43c90a6bd272b8ccc19980e51e7a3/1.png)

Untuk tampilan jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman,perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman tidak dapat ditampilkan : 

![2](/uploads/6e6487fc833ddc2397ff5330e231df41/2.png)

### Kendala 
Untuk menampilkan perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman tidak dapat ditampilkan

## nomor 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

1. Membuat program C dengan nama kalian.c yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

2. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. (Catatan: wajib menerapkan konsep shared memory)

3. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

4. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. Dokumentasikan dan sampaikan saat demo dan laporan resmi.

### 2.1
```C
void generateMatrix() {
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL1; j++) {
      mat1[i][j] = rand() % 5 + 1;
    }
  }
  for (int i = 0; i < ROW2; i++) {
    for (int j = 0; j < COL2; j++) {
      mat2[i][j] = rand() % 4 + 1;
    }
  }
}

while (1) {
  int flag = 0;
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
    val[i][j] = 0;
    for (int k = 0; k < ROW2; k++) {
      val[i][j] += (mat1[i][k] * mat2[k][j]);
      if (val[i][j] > 20) {
      flag = 1;
      generateMatrix();
      break;
      }
    }
    if (flag == 1)
      break;
    }
    if (flag == 1)
    break;
  }
  if (flag == 0) {
    break;
  }
}

```
Pada fungsi generateMatrix membuat sebuah matrix 1 dan matrix 2 dimana akan membuat nilai random dan dimodulo dengan 5 untuk matrix pertama dan 4 matrix kedua kemudian ditambahkan satu untuk menghindari hasil 0.


### 2.2
```C
  key_t key = ftok("./kalian.c", 'R');
  int shmid = shmget(key, 4 * 5 * sizeof(int), IPC_CREAT | 0666);
  if (shmid < 0) {
    perror("shmget");
    exit(1);
  }
  val = shmat(shmid, NULL, 0);
```
Potongan kode diatas untuk menggunakan shared memory yang telah diinisiasi oleh program kalian.c, kemudian val menjadi variabel yang menyimpan value dari kalian.c

### 2.3
```C
  for (int i = 0; i < ROW1; i++) {
    for (int j = 0; j < COL2; j++) {
      int *args = (int *)malloc(3 * sizeof(int));
      args[0] = val[i][j];
      args[1] = i;
      args[2] = j;
      pthread_create(&factorial[i * COL2 + j], NULL, factorialThread,
                     (void *)args);
    }
  }
```
Pada potongan kode diatas membuat thread dan menggunakan fungsi factorialThread untuk melakukan perhitungan faktorial.

```C
void *factorialThread(void *arg) {
  int *args = (int *)arg;
  int val = args[0];
  int i = args[1];
  int j = args[2];
  long long int result = 1;
  for (int i = 1; i <= val; i++) {
    result *= i;
  }
  factorialArr[i][j] = result;
  free(arg);
  pthread_exit(NULL);
}
```
Pada potongan kode diatas melakukan perhitungan faktorial dengan loop dari 1 hingga ke value tersebut.

### 2.4 
```C
 for (int i = 0; i < ROW1; i++) {
  for (int j = 0; j < COL2; j++) {
   long long int res = factorialWithoutThread(val[i][j]);
   factorialArr[i][j] = res;
  }
 }

```
Pada potongan kode diatas melakukan perhitungan faktorial tanpa membuat sebuah thread sehingga program tersebut hanyalah single thread.

### 2.hasil
berikut merupakan isi dari folder setelah menjalankan kalian.c, cinta.c, sisop.c :
![Screenshot_2023-05-12_at_19.38.57](/uploads/e58346be1b2796f78f8e32722f3dbd3e/Screenshot_2023-05-12_at_19.38.57.png)

## nomor 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

### 3.1 Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

```C
    //Pada stream.c
    if (msgrcv(msgid, &msg, sizeof(struct message), 1, 0) == -1) {
      perror("msgrcv");
      exit(1);
    }

    //Pada user.c
    msgsnd(msgid, &msg, sizeof(msg), 0);
```
Potongan dengan komen stream.c adalah kode untuk menerima msg pada msgid, sedangkan pada user.c menggunakan `msgsnd` untuk mengirimkan msg ke msgid

### 3.2 User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json, meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.

```C
    field = strstr(buffer, "\"method\":");
    if (field != NULL) {
      value = strstr(field, ": ") + 3;
      method = strtok(value, "\"");
      strncpy(playlist[i].method, method, MAX_SONG_LENGTH);
    }
```
Potongan kode diatas untuk mengambil field method pada file json dengan menggunakan `strstr` dan menghapus yang tidak terpakai pada string field dan value.

```C
    // Find "song" field
    field = strstr(buffer, "\"song\":");
    if (field != NULL) {
      value = strstr(field, ": ") + 3;
      song = strtok(value, "\"");
      transformed_song = malloc(MAX_SONG_LENGTH);
      if (strcmp(playlist[i].method, "rot13") == 0) {
        strncpy(transformed_song, song, MAX_SONG_LENGTH);
        rot13(transformed_song);
        strcat(transformed_song, "\n");
        ;
      } else if (strcmp(playlist[i].method, "base64") == 0) {
        char *result = base64_decode(song);
        sprintf(transformed_song, "%s\n", result);
      } else if (strcmp(playlist[i].method, "hex") == 0) {
        char *result = hexToString(song);
        sprintf(transformed_song, "%s\n", result);
      }
      strncpy(temp_songs[j], transformed_song, MAX_SONG_LENGTH);
      free(transformed_song);
      strncpy(playlist[i].song, song, MAX_SONG_LENGTH);
      j++;
    }
```
Potongan kode diatas untuk mengambil song field pada file json, dan setela didapatkan. Lagu tersebut akan diubah berdasarkan method fieldnya. `sprintf` digunakan untuk mengubah format string pada lagu.

### 3.3 Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

```C
  system("sort -f -k1,1 playlist.txt");
```
Potongan kode diatas untuk melakukan command `sort` dengan flag `-f`yang berarti case insensitive, `-k1,1` yang berarti sort berdasarkan hanya kolom pertama.

### 3.4 User juga dapat mengirimkan perintah PLAY <SONG>

```C
  sprintf(linuxCmd, "grep '^%s' playlist.txt | grep -v ' - %s'", args, args);
```
Potongan kode diatas untuk melakukan command `grep` untuk mencari SONG yang telah diinput, command `^grep` untuk mencari first string yang match. grep kedua untuk menexclude inputan yang didahului dengan '-' 

```C
      while (fgets(buffer, sizeof(buffer), pipe) != NULL) {
        char temp_buf[100];
        i++;
        sprintf(temp_buf, "%d. %s", i, buffer);
        strcat(temp_res, temp_buf);
      }
      if (i > 1) {
        sprintf(response, "THERE ARE \"%d\" SONG CONTAINING \"%s\":\n%s", i,
                args, temp_res);
      } else if (i == 1) {
        sprintf(response, "USER %d PLAYING \"%s\"", sender_id, buffer);
      } else {
        sprintf(response, "THERE IS NO SONG CONTAINING \"%s\"", args);
      }
      printf("%s", response);
      pclose(pipe);
    }
```
Potongan kode diatas untuk mengolah hasil grep yang telah dilakukan, saat `i = 0` maka tidak ada song yang dicari oleh user. sedangkan jika `i == 1` maka user telah menginput song yang sudah spesifik dan jika `i > 1` Maka hasil dari grep tersebut mereturn banyak song yang diinput oleh user

### 3.5 User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut: 

1). User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>
```C
    snprintf(linuxCmd, sizeof(linuxCmd), "grep -x \"%s\" playlist.txt", args);
```
Potongan kode diatas melakukan command `grep` dengan flag `-x` yang berarti input harus unik atau belum terdapat pada playlist.

2). User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”
```C
    int flag = 0;
    while (fgets(buffer, sizeof(buffer), pipe) != NULL) {
      flag++;
      break;
    }
    pclose(pipe);
    if (flag == 1)
      printf("SONG ALREADY ON PLAYLIST\n");
```
Potongan kode diatas jika pipe bernilai tidak NULL, maka command `grep -x` mereturn song, sehingga 'flag bernilai 1' dimana SONG ALREADY ON PLAYLIST

### 3.6 Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.

Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
```C
    if (strlen(pid[0]) == 0) {
      strcpy(pid[0], temp_pid);
      if (semop(semid, &sem_lock, 1) == -1) {
        perror("semop");
        exit(1);
      }
    } else if (strlen(pid[1]) == 0 && strcmp(temp_pid, pid[0]) != 0) {
      strcpy(pid[1], temp_pid);
      if (semop(semid, &sem_lock, 1) == -1) {
        perror("semop");
        exit(1);
      }
    } else {
      if (strcmp(temp_pid, pid[0]) != 0 && strcmp(temp_pid, pid[1]) != 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
      }
    }
```
Potongan kode diatas jika pid user pertama dan kedua sudah terisi, maka semaphore telah dilock. Jika terdapat user ketiga akan di print STREAM SYSTEM OVERLOAD.

### 3.7 Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

```C
    void userCommand(char cmd[], char args[], int msgid, int sender_id) {
      else {
        printf("UNKNOWN COMMAND");
      }
    }
```
Potongan kode diatas jika cmd tidak terdaftar maka akan print UNKNOWN COMMAND pada layar stream.c

### 3.hasil

Berikut merupakan tampilan pada stream dan user saat mengirimkan dan menerima command (tampilan pada stream berurutan dengan command user dibawah):

list song:
![Screenshot_2023-05-12_at_14.56.13](/uploads/d51adf16f2a9392bb3ae54b8c2b581dd/Screenshot_2023-05-12_at_14.56.13.png)

play command:
![Screenshot_2023-05-12_at_14.56.23](/uploads/3915f09b23af9b5958211b89b8b2b32c/Screenshot_2023-05-12_at_14.56.23.png)

third user command:
![Screenshot_2023-05-12_at_14.56.32](/uploads/2b652bcfe7ca0eab3663d38aacf5ff66/Screenshot_2023-05-12_at_14.56.32.png)

unknown command:
![Screenshot_2023-05-12_at_14.58.21](/uploads/c8b724ed3773c6f4f995cde941321dc8/Screenshot_2023-05-12_at_14.58.21.png)

tampilan stream:
![Screenshot_2023-05-12_at_14.58.19](/uploads/c6c0298529b9c2fdb5acf3b74927af1b/Screenshot_2023-05-12_at_14.58.19.png)

## nomor 4
Pada nomor ini kita diminta untuk membantu Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt.

### soal 4.a
Pada poin pertama ini kita diminta untuk melakukan zip dan unzip file "hehe.zip" dalam kode bernama unzip.C , yang perlu dilakukan hanya , menjalankan program unzip.c pada folder kamu, pertama lakukan 

```c
gcc unzip.c -o unzip -lzip
```
lalu 
```c
./unzip
```
Setelah itu file hehe.zip akan ter download dan akan di unzip. Isi file tersebut ada folder files, extensions.txt dan max.txt. 

### soal 4.b
Pada poin kedua kita diminta untuk membuat program categorize.c , jadi intinya pada program tersebut kita diminta untuk mengumpulkan file dari folder "files" dan dijadikan satu pada folder "categorized", lalu dikelompokan pada folder sesuai list extension pada file "extensions.txt", untuk file yang tidak ada pada list extension akan dimasukan ke folder "other" 

// menjalankan program categorize.c 
![Screenshot_jalankan_categorize](/uploads/19625f4d10e170bf3e650c4fd11ce866/Screenshot_jalankan_categorize.jpg)

// hasil setelah dikelompokan 
![Screenshot_berhasil_categorized](/uploads/533a37bd5cd31b2ee64e4ebe9b0478c2/Screenshot_berhasil_categorized.jpg)

Permintaan selanjutnya adalah pada file "max.txt" terdapat jumlah maksimum tiap sub folder, yaitu 10 file. Jika sudah penuh dibuat folder baru dengan format extension (2), extension (3), dan seterusnya.

// file berhasil bertambah jika sudah penuh 
![Screenshot_file_berhasil_dikumpulkan](/uploads/63bb5ed190fa195b76bc439240bb6fb4/Screenshot_file_berhasil_dikumpulkan.jpg)

Setelah itu kita juga diminta untuk membuat output di terminal pada saat menjalankan program nya yaitu banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut
```
  extension_a : banyak_file
  extension_b : banyak_file
  extension_c : banyak_file
  other : banyak_file
```

//output pada terminal 
![Screenshot_jalankan_categorize](/uploads/19625f4d10e170bf3e650c4fd11ce866/Screenshot_jalankan_categorize.jpg)

Lalu kita diminta untuk membuat file yaitu "log.txt" berisi log dari folder "categorize" dengan ketentuan
```
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
```

// isi log.txt

![Screenshot_isi_log.txt](/uploads/59dbb8117f3dc57f794b3b0daa6dfd1d/Screenshot_isi_log.txt.jpg)

### soal 4.c
Pada poin ketiga kita diminta untuk membuat program "logchecker.c" yang terdapat 3 perintah yaitu menghitun banyak ACCESSED yang dilakukan, membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending, dan Untuk menghitung banyaknya total file tiap extension, terurut secara ascending. Saya hanya melakukan 2 perintah saja, membuat file list dan menghitung total tiap extension

// logchecker.c dijalankan serta banyak nya extension 
![Screenshot_menjalankan_logchecker](/uploads/b9ba115e90aa24a2a3fa2a805bfcdc16/Screenshot_menjalankan_logchecker.jpg)

// isi dari file list.txt
![Screenshot_list.txt](/uploads/7c05c34aea412d617edba94d70299dd5/Screenshot_list.txt.jpg)

## kendala
1. cukup lama stuck dibagian cron karena tidak jalan-jalan tapi akhirnya bisa jalan juga
2. Soal yang diberikan susah, dan terlalu muter-muter :)

