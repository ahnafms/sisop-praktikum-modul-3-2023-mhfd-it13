#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>

#define LOG_FILE "log.txt"

// Fungsi buat mendapatkan ekstensi file-nya
const char *ambil_file(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename) return "";
    return dot + 1;
}

// Fungsi buat menuliskan log
void Tulis_Log(const char *action, const char *path) {
    time_t now;
    char timestamp[20];
    FILE *file;

    time(&now);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));

    file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("Gagal Buka Log File ");
        exit(EXIT_FAILURE);
    }

    fprintf(file, "%s %s [%s]\n", timestamp, action, path);

    fclose(file);
}

// Fungsi buat mengkategorikan file
void kategori_file(char *filename, const char *ext) {
    char categorized_dir[255] = "categorized/";
    char new_folder[255];
    struct stat st = {0};

    if (stat(categorized_dir, &st) == -1) {
        mkdir(categorized_dir, 0755);
    }

    snprintf(new_folder, sizeof(new_folder), "categorized/%s", ext);

    if (stat(new_folder, &st) == -1) {
        mkdir(new_folder, 0755);
    }

    int nomor_folder = 1;
    int max_files = 10;
    char nama_folder[255];

    while (1) {
        snprintf(nama_folder, sizeof(nama_folder), "%s (%d)", new_folder, nomor_folder);
        DIR *dir = opendir(nama_folder);
        if (dir) {
            int file_count = 0;
            struct dirent *entry;

            while ((entry = readdir(dir)) != NULL) {
                if (entry->d_type == DT_REG) {
                    file_count++;
                }
            }
            closedir(dir);

            if (file_count >= max_files) {
                nomor_folder++;
            } else {
                break;
            }
        } else if (ENOENT == errno) {
            mkdir(nama_folder, 0755);
            break;
        } else {
            perror("Error Buka Direktori mas bro");
            return;
        }
    }

    char file_path_baru[255];
    snprintf(file_path_baru, sizeof(file_path_baru), "%s/%s", nama_folder, filename);
    rename(filename, file_path_baru);

    char src_path[255];
    snprintf(src_path, sizeof(src_path), "%s/%s", ".", filename);
    char dst_path[255];
    snprintf(dst_path, sizeof(dst_path), "%s/%s", new_folder, nama_folder + strlen(new_folder) + 1);
    Tulis_Log("MOVED", dst_path);
}

int main() {
    DIR *d;
    struct dirent *dir;
    d = opendir(".");

    if (d) {
        while ((dir = readdir(d)) != NULL) {
            const char *ext = ambil_file(dir->d_name);
            if (strcmp(ext, "jpg") == 0 || strcmp(ext, "txt") == 0 || strcmp(ext, "js") == 0 ||
                strcmp(ext, "py") == 0 || strcmp(ext, "png") == 0 || strcmp(ext, "emc") == 0 ||
                strcmp(ext, "xyz") == 0) {
                kategori_file(dir->d_name, ext);
            } else if (strlen(ext) > 0) {
                kategori_file(dir->d_name, "other");
            }
        }
        closedir(d);
    }

// Buat mencetak jumlah file tiap ekstensi
    int jpg_count = 0, txt_count = 0, js_count = 0, py_count = 0, png_count = 0, emc_count = 0, xyz_count = 0, other_count = 0;

    d = opendir("categorized");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                DIR *sub_d;
                struct dirent *sub_dir;
                char sub_dir_name[255];
                snprintf(sub_dir_name, sizeof(sub_dir_name), "categorized/%s", dir->d_name);
                sub_d = opendir(sub_dir_name);
                if (sub_d) {
                    while ((sub_dir = readdir(sub_d)) != NULL) {
                        if (sub_dir->d_type == DT_REG) {
                            const char *ext = ambil_file(sub_dir->d_name);
                            if (strcmp(ext, "jpg") == 0) {
                                jpg_count++;
                            } else if (strcmp(ext, "txt") == 0) {
                                txt_count++;
                            } else if (strcmp(ext, "js") == 0) {
                                js_count++;
                            } else if (strcmp(ext, "py") == 0) {
                                py_count++;
                            } else if (strcmp(ext, "png") == 0) {
                                png_count++;
                            } else if (strcmp(ext, "emc") == 0) {
                                emc_count++;
                            } else if (strcmp(ext, "xyz") == 0) {
                                xyz_count++;
                            } else {
                                other_count++;
                            }
                        }
                    }
                    closedir(sub_d);
                }
            }
        }
        closedir(d);

        // Buat mencetak jumlah file tiap ekstensi secara urut
        printf("jpg: %d\n", jpg_count);
        printf("txt: %d\n", txt_count);
        printf("js: %d\n", js_count);
        printf("py: %d\n", py_count);
        printf("png: %d\n", png_count);
        printf("emc: %d\n", emc_count);
        printf("xyz: %d\n", xyz_count);
        printf("other: %d\n", other_count);

    // Buat nambahkan file log.txt
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char log_time[20];
    strftime(log_time, sizeof(log_time), "%d-%m-%Y %H:%M:%S", &tm);

    FILE *fp;
    fp = fopen("log.txt", "a");

    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                char dir_path[255];
                snprintf(dir_path, sizeof(dir_path), "%s/%s", ".", dir->d_name);
                fprintf(fp, "%s ACCESSED [%s]\n", log_time, dir_path);

                DIR *sub_d;
                struct dirent *sub_dir;
                char sub_dir_path[255];
                snprintf(sub_dir_path, sizeof(sub_dir_path), "%s/%s", ".", dir->d_name);
                sub_d = opendir(sub_dir_path);

                if (sub_d) {
                    while ((sub_dir = readdir(sub_d)) != NULL) {
                        if (sub_dir->d_type == DT_REG) {
                            const char *ext = ambil_file(sub_dir->d_name);
                            char src_path[255];
                            snprintf(src_path, sizeof(src_path), "%s/%s", dir_path, sub_dir->d_name);
                            char dst_path[255];
                            snprintf(dst_path, sizeof(dst_path), "categorized/%s/%s", ext, sub_dir->d_name);

                            kategori_file(src_path, ext);
                            fprintf(fp, "%s MOVED %s file : %s > %s\n", log_time, ext, src_path, dst_path);
                        }
                    }
                    closedir(sub_d);
                }
            }
        }
        closedir(d);
    }

    d = opendir("categorized");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                char dir_path[255];
                snprintf(dir_path, sizeof(dir_path), "%s/%s", "categorized", dir->d_name);
                fprintf(fp, "%s MADE [%s]\n", log_time, dir_path);
            }
        }
        closedir(d);
    }

    fclose(fp);
} else {
    perror("Error Buka Direktori mas bro");

    }

    return 0;
}
