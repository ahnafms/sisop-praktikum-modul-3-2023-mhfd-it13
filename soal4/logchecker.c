#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>

void Tambah_File(char* filename);
void List_Folder(char* filename);
int Hitung_Ekstensi(char** files, int Hitung_File);

int main() {
    char* log_file = "list.txt";
    List_Folder(log_file);

    FILE* fp = fopen(log_file, "r");
    if (fp == NULL) {
        printf("Gagal buka log file mas bro\n");
        exit(1);
    }

    char buffer[1024];
    int Hitung_File = 0;
    while (fgets(buffer, 1024, fp) != NULL) {
        Tambah_File(buffer);
        Hitung_File++;
    }

    printf("Total files: %d\n", Hitung_File);
    fclose(fp);

    char* files[] = {"file1.txt", "file2.c", "file3.h", "file4.txt", "file5.cpp"};
    int extension_count = Hitung_Ekstensi(files, 5);
    printf("Total extensions: %d\n", extension_count);

    return 0;
}

void Tambah_File(char* filename) {
    char* file_ext = strrchr(filename, '.');
    if (file_ext != NULL) {
        file_ext++;
        printf("File extension: %s\n", file_ext);
    }
}

void List_Folder(char* log_file) {
    char current_dir[FILENAME_MAX];
    DIR *dir_ptr;
    struct dirent *dir_entry;
    struct stat file_stat;
    FILE *file_ptr;
    
    if (getcwd(current_dir, sizeof(current_dir)) == NULL) {
        perror("Direktori Error mas bro");
        return;
    }
    
    dir_ptr = opendir(current_dir);
    if (dir_ptr == NULL) {
        perror("Error Buka Direktori mas bro");
        return;
    }
    
    file_ptr = fopen(log_file, "w");
    if (file_ptr == NULL) {
        perror("Error Buka Log File mas bro");
        return;
    }
    
    while ((dir_entry = readdir(dir_ptr)) != NULL) {
        if (strcmp(dir_entry->d_name, ".") == 0 || strcmp(dir_entry->d_name, "..") == 0) {
            continue;
        }
        
        char file_path[FILENAME_MAX];
        snprintf(file_path, sizeof(file_path), "%s/%s", current_dir, dir_entry->d_name);
        
        if (stat(file_path, &file_stat) == -1) {
            perror("Error ngambil Status mas bro");
            continue;
        }
        
        if (S_ISDIR(file_stat.st_mode)) {
            fprintf(file_ptr, "%s/\n", dir_entry->d_name);
        }
    }
    
    closedir(dir_ptr);
    fclose(file_ptr);
}

int Hitung_Ekstensi(char** files, int Hitung_File) {
    int extension_count = 0;
    for (int s = 0; s < Hitung_File; s++) {
        char* file_ext = strrchr(files[s], '.');
        if (file_ext != NULL) {
            extension_count++;
        }
    }
    return extension_count;
}
