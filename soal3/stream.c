#include <ctype.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/types.h>

#define JSON_FILE "./song-playlist.json"
#define MAX_MSG_SIZE 1024
#define MAX_SONG_LENGTH 256
#define MAX_USERS 2
#define BASE64_CHARS                                                           \
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="

struct message {
  long type;
  char text[MAX_MSG_SIZE];
  pid_t pid;
};

struct message msg;

typedef struct {
  char method[MAX_SONG_LENGTH];
  char song[MAX_SONG_LENGTH];
} PlaylistItem;

// function used for DECRYPT
void rot13(char *s);
char *hexToString(char *hex_str);
char *base64_decode(const char *input);
int cmpfunc(const void *a, const void *b);

void DECRYPT() {
  FILE *fp, *fp_playlist;
  char buffer[1024];
  char temp_songs[1000][100];
  PlaylistItem playlist[10];
  int i = 0, j = 0;
  fp_playlist = fopen("./playlist.txt", "w");
  fp = fopen("./song-playlist.json", "r");
  if (fp == NULL) {
    fprintf(stderr, "Failed to open file\n");
    exit(1);
  }

  while (fgets(buffer, sizeof(buffer), fp) != NULL) {
    char *field, *value, *method, *song, *transformed_song;

    field = strstr(buffer, "\"method\":");
    if (field != NULL) {
      value = strstr(field, ": ") + 3;
      method = strtok(value, "\"");
      strncpy(playlist[i].method, method, MAX_SONG_LENGTH);
    }
    // Find "song" field
    field = strstr(buffer, "\"song\":");
    if (field != NULL) {
      value = strstr(field, ": ") + 3;
      song = strtok(value, "\"");
      transformed_song = malloc(MAX_SONG_LENGTH);
      if (strcmp(playlist[i].method, "rot13") == 0) {
        strncpy(transformed_song, song, MAX_SONG_LENGTH);
        rot13(transformed_song);
        strcat(transformed_song, "\n");
        ;
      } else if (strcmp(playlist[i].method, "base64") == 0) {
        char *result = base64_decode(song);
        sprintf(transformed_song, "%s\n", result);
      } else if (strcmp(playlist[i].method, "hex") == 0) {
        char *result = hexToString(song);
        sprintf(transformed_song, "%s\n", result);
      }
      strncpy(temp_songs[j], transformed_song, MAX_SONG_LENGTH);
      free(transformed_song);
      strncpy(playlist[i].song, song, MAX_SONG_LENGTH);
      j++;
    }
  }
  qsort(temp_songs, 1000, 100, cmpfunc);
  for (int i = 0; i < 1000; i++) {
    fputs(temp_songs[i], fp_playlist);
  }
  fclose(fp_playlist);
  fclose(fp);
}

void userCommand(char cmd[], char args[], int msgid, int sender_id) {
  if (strcmp(cmd, "list") == 0) {
    system("sort -f -k1,1 playlist.txt");
  }

  else if (strcmp(cmd, "play") == 0) {
    int len = strcspn(args, "\n");
    args[len] = '\0';
    char buffer[1024] = {'\0'}, linuxCmd[100], temp_res[1000] = {'\0'},
         response[1024] = {'\0'};
    sprintf(linuxCmd, "grep '^%s' playlist.txt | grep -v ' - %s'", args, args);
    FILE *pipe = popen(linuxCmd, "r");
    if (pipe == NULL) {
      sprintf(response, "ERROR: Failed to open pipe\n");
    } else {
      int i = 0;
      while (fgets(buffer, sizeof(buffer), pipe) != NULL) {
        char temp_buf[100];
        i++;
        sprintf(temp_buf, "%d. %s", i, buffer);
        strcat(temp_res, temp_buf);
      }
      if (i > 1) {
        sprintf(response, "THERE ARE \"%d\" SONG CONTAINING \"%s\":\n%s", i,
                args, temp_res);
      } else if (i == 1) {
        sprintf(response, "USER %d PLAYING \"%s\"", sender_id, buffer);
      } else {
        sprintf(response, "THERE IS NO SONG CONTAINING \"%s\"", args);
      }
      printf("%s", response);
      pclose(pipe);
    }
  } else if (strcmp(cmd, "add") == 0) {
    char temp_args[1024], linuxCmd[1024], buffer[1024], res[100];
    snprintf(linuxCmd, sizeof(linuxCmd), "grep -x \"%s\" playlist.txt", args);
    FILE *pipe = popen(linuxCmd, "r");
    int flag = 0;
    while (fgets(buffer, sizeof(buffer), pipe) != NULL) {
      flag++;
      break;
    }
    pclose(pipe);
    if (flag == 1)
      printf("SONG ALREADY ON PLAYLIST\n");
    else if (flag == 0) {
      FILE *fp = fopen("playlist.txt", "a");
      if (fp == NULL) {
        printf("Error opening file\n");
      } else {
        fprintf(fp, "%s\n", args);
        sprintf(res, "USER %d ADD %s", sender_id, args);
        printf("%s", res);
        fclose(fp);
      }
    }
  } else {
    printf("UNKNOWN COMMAND");
  }
}

int main() {
  key_t msg_key, sem_key;
  int msgid;
  char pid[2][100], temp_pid[100];
  memset(pid, 0, sizeof(pid));
  msg_key = ftok("stream", 'm');
  sem_key = ftok("stream", 's');
  struct sembuf sem_lock = {0, -1, SEM_UNDO};
  struct sembuf sem_unlock = {0, 1, SEM_UNDO};

  if (msg_key == -1) {
    perror("ftok");
    exit(1);
  }

  int semid = semget(sem_key, 1, 0666 | IPC_CREAT);
  if (semid == -1) {
    perror("semget");
    exit(1);
  }

  union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
  } arg;
  arg.val = MAX_USERS;
  if (semctl(semid, 0, SETVAL, arg) == -1) {
    perror("semctl");
    exit(1);
  }
  msgid = msgget(msg_key, 0666 | IPC_CREAT);
  if (msgid == -1) {
    perror("msgget");
    exit(1);
  }
  DECRYPT();
  printf("DECRYPT...\n");
  while (1) {
    char *command, *args;
    char temp_msg[100];
    if (msgrcv(msgid, &msg, sizeof(struct message), 1, 0) == -1) {
      perror("msgrcv");
      exit(1);
    }
    sprintf(temp_pid, "%d", msg.pid);
    if (strlen(pid[0]) == 0) {
      strcpy(pid[0], temp_pid);
      if (semop(semid, &sem_lock, 1) == -1) {
        perror("semop");
        exit(1);
      }
    } else if (strlen(pid[1]) == 0 && strcmp(temp_pid, pid[0]) != 0) {
      strcpy(pid[1], temp_pid);
      if (semop(semid, &sem_lock, 1) == -1) {
        perror("semop");
        exit(1);
      }
    } else {
      if (strcmp(temp_pid, pid[0]) != 0 && strcmp(temp_pid, pid[1]) != 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
      }
    }
    strcpy(temp_msg, msg.text);
    command = strtok(temp_msg, " ");
    for (int i = 0; i < strlen(command); i++) {
      command[i] = tolower(command[i]);
    }
    args = strtok(NULL, "");
    if (strcmp(pid[0], temp_pid) == 0 || strcmp(pid[1], temp_pid) == 0) {
      if (semop(semid, &sem_unlock, 1) == -1) {
        perror("semop");
        exit(1);
      }
      userCommand(command, args, msgid, msg.pid);
      if (semop(semid, &sem_lock, 1) == -1) {
        perror("semop");
        exit(1);
      }
    }
  }
  if (semctl(semid, 0, IPC_RMID) == -1) {
    perror("semctl");
    exit(1);
  }
  return 0;
}

char *base64_decode(const char *input) {
  size_t input_len = strlen(input);
  size_t output_len = input_len / 4 * 3;
  if (input[input_len - 1] == '=') {
    output_len--;
  }
  if (input[input_len - 2] == '=') {
    output_len--;
  }
  char *output = (char *)malloc(output_len + 1);
  if (output == NULL) {
    return NULL;
  }
  memset(output, 0, output_len + 1);
  size_t i, j;
  for (i = 0, j = 0; i < input_len; i += 4, j += 3) {
    unsigned int a = strchr(BASE64_CHARS, input[i]) - BASE64_CHARS;
    unsigned int b = strchr(BASE64_CHARS, input[i + 1]) - BASE64_CHARS;
    unsigned int c = input[i + 2] == '='
                         ? 0
                         : strchr(BASE64_CHARS, input[i + 2]) - BASE64_CHARS;
    unsigned int d = input[i + 3] == '='
                         ? 0
                         : strchr(BASE64_CHARS, input[i + 3]) - BASE64_CHARS;
    unsigned int value = (a << 18) | (b << 12) | (c << 6) | d;
    output[j] = (char)((value >> 16) & 0xFF);
    if (input[i + 2] != '=') {
      output[j + 1] = (char)((value >> 8) & 0xFF);
    }
    if (input[i + 3] != '=') {
      output[j + 2] = (char)(value & 0xFF);
    }
  }
  return output;
}

char *hexToString(char *hex_str) {
  int len = strlen(hex_str);
  if (len % 2 != 0) {
    fprintf(stderr, "Hex string must have an even number of characters\n");
    return NULL;
  }

  char *ascii_str = (char *)malloc((len / 2) + 1);
  if (ascii_str == NULL) {
    fprintf(stderr, "Memory allocation failed\n");
    return NULL;
  }

  for (int i = 0; i < len; i += 2) {
    char hex_byte[3];
    strncpy(hex_byte, &hex_str[i], 2);
    hex_byte[2] = '\0';
    long int val = strtol(hex_byte, NULL, 16);
    ascii_str[i / 2] = (char)val;
  }
  ascii_str[len / 2] = '\0';

  return ascii_str;
}

void rot13(char *s) {
  char *p = s;
  while (*p != '\0') {
    if (*p >= 'a' && *p <= 'm') {
      *p += 13;
    } else if (*p >= 'n' && *p <= 'z') {
      *p -= 13;
    } else if (*p >= 'A' && *p <= 'M') {
      *p += 13;
    } else if (*p >= 'N' && *p <= 'Z') {
      *p -= 13;
    }
    p++;
  }
}

int cmpfunc(const void *a, const void *b) {
  return strcmp((const char *)a, (const char *)b);
}
