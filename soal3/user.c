#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <errno.h>

#define MAX_MSG_SIZE 1024

struct message {
  long type;
  char text[MAX_MSG_SIZE];
  pid_t pid;
};

int main() {
  int msgid, resid;
  key_t msg_key;
  struct message msg, response;

  msg_key = ftok("stream", 'm');
  msgid = msgget(msg_key, 0666 | IPC_CREAT);
  if (msgid < 0) {
    perror("msgget");
    exit(EXIT_FAILURE);
  }
  msg.pid = getpid();
  while (1) {
    printf("Enter a command:\n");
    fgets(msg.text, MAX_MSG_SIZE, stdin);
    msg.type = 1;
    msgsnd(msgid, &msg, sizeof(msg), 0);
    printf("Command sent\n");
  }

  return 0;
}
